import requests
import json
from pprint import pprint

def get_movies(movie_tag):
    '''
    get movie's brief info by movie tag 
    '''

    movies_list = []
    # 定义请求url
    url = "https://movie.douban.com/j/search_subjects"
    # 定义请求头
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36"
    }
    # 循环构建请求参数并且发送请求
    for page_start in range(0, 1000, 20):
        params = {
            "type": "movie",
            "tag": movie_tag,
            "sort": "time",
            "page_limit": "20",
            "page_start": page_start
        }
        try:
            response = requests.get(
                url=url,
                headers=headers,
                params=params
            )
            # 方式一:直接转换json方法
            # results = response.json()
            # 方式二: 手动转换
            # 获取字节串
            content = response.content
            # 转换成字符串
            string = content.decode('utf-8')
            # 把字符串转成python数据类型
            results = json.loads(string)
        except:
            print('over size!')
        if len(results) == 0:
            break
        # 解析结果
        for movie in results["subjects"]:
    #         print(movie["title"], movie["rate"])
            movies_list.append(movie)

    return movies_list

def get_all_movies():
    '''
    get all movies brief info with movies tag
    '''

    movie_tags = ['热门']

    all_movies = []

    for tag in movie_tags:
        all_movies.append(get_movies(tag)) 
    return all_movies

if __name__ == '__main__':

    # all_movies = get_all_movies()
    # pprint(all_movies)
    # print(len(all_movies))

    # get all movie's brief info & write to m.json
    with open('m.json', 'w') as f:
        json.dump(get_all_movies(), f)