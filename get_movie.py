from fake_useragent import UserAgent
import time
import random
import requests
import json
from bs4 import BeautifulSoup as BS
from pprint import pprint
import re
from bs4 import BeautifulSoup

# 更新 UserAgent 数据, 因 fake_useragent 会引用在线资源，有时会因网络超时报错，可禁用宿主缓存（ua = UserAgent(use_cache_server=False)）
ua = UserAgent()
ua.update()

def get_movie(movies_list, movie_id):
    '''
    get movie's info with movie's id from movie page & return json format
    '''

    # add movie's brief info from movies_list
    movie_json = [x for x in movies_list if x['id'] == movie_id][0]

    # 定义请求url
    url = f"https://movie.douban.com/subject/{movie_id}"

# 定义随机的请求头
    headers = {'User-Agent': ua.random}
    page = requests.get(url, headers=headers).content
    soup = BS(page, 'html.parser')

#    print(movie_id)
    # get movie's date and region
    date = soup.find_all('span', attrs={'property': 'v:initialReleaseDate'})
    if len(date) != 0:
        date_txt = date[0].text
#        print(date_txt)
        x = re.search('(\d\d\d\d-\d\d-\d\d|\d\d\d\d)\((.*)\)', date_txt)
        movie_json['date'] = x.group(1)
        movie_json['region'] = x.group(2)
    else:
        movie_json['date'] = ''
        movie_json['region'] = ''

    # get movie's rating people number
    rate = soup.find_all('span', attrs={'property': 'v:votes'})
    if len(rate) != 0:
        print(rate[0].text)
        movie_json['rating_people'] = int(rate[0].text)
    else:
        movie_json['rating_people'] = 0

    return movie_json

def get_all():
    '''
    get all movies info & return list
    '''
    with open('m.json', 'r') as f:
        m = json.load(f)

    movies = []
    for movie in m:
        movies.append(get_movie(m, movie['id']))
# 每次循环任务暂停 1 到 5 秒之间的随机秒数
        time.sleep(random.uniform(1, 8))

    return movies

if __name__ == '__main__':

    # write all movies info to movies.json
    with open('movies.json', 'w') as f:
        json.dump(get_all(), f)

    # with open('m.json', 'r') as f:
    #     m = json.load(f)
    # pprint(get_movie(m, '34964056'))
    # pprint(get_movie(m, '27599401'))